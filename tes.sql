/*
 Navicat MySQL Data Transfer

 Source Server         : localhost
 Source Server Type    : MySQL
 Source Server Version : 80020
 Source Host           : localhost:5369
 Source Schema         : tes

 Target Server Type    : MySQL
 Target Server Version : 80020
 File Encoding         : 65001

 Date: 14/01/2022 21:04:01
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for lamp
-- ----------------------------
DROP TABLE IF EXISTS `lamp`;
CREATE TABLE `lamp`  (
  `cross_number` int NULL DEFAULT NULL,
  `lamp_num` int NOT NULL COMMENT '灯组编号',
  `direction` int NULL DEFAULT NULL,
  `flow` int NULL DEFAULT NULL
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for phase
-- ----------------------------
DROP TABLE IF EXISTS `phase`;
CREATE TABLE `phase`  (
  `cross_number` int NOT NULL,
  `phase_num` int NOT NULL,
  `lamp_num` int NOT NULL COMMENT '灯组编号',
  `green` int NOT NULL,
  `yellow` int NOT NULL,
  `red` int NOT NULL,
  `min_green` int NOT NULL,
  UNIQUE INDEX `uqi_phase_number`(`phase_num`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for security
-- ----------------------------
DROP TABLE IF EXISTS `security`;
CREATE TABLE `security`  (
  `cross_number` int NULL DEFAULT NULL,
  `phase_num` int NULL DEFAULT NULL,
  `security_green` int NULL DEFAULT NULL,
  `security_green_flash` int NULL DEFAULT NULL,
  `security_yellow` int NULL DEFAULT NULL,
  `security_red` int NULL DEFAULT NULL,
  UNIQUE INDEX `uqi_phase_number`(`phase_num`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_bin ROW_FORMAT = Dynamic;

SET FOREIGN_KEY_CHECKS = 1;
