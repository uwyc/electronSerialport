module.exports = {
  pluginOptions: {
    electronBuilder: {
      externals: ['serialport'],
      nodeIntegration: true,
      preload: 'src/preload.js'
    }
  }
}