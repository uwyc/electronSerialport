import {
  takeVersion,
  takeSend,
  takeReceive,
  takeDataChainCode,
  takeRegion,
  takeCrossNumber,
  takeOperateType,
  takeObjectId,
  takeSerialNumber,
  takeObjectNumber,
  takeByteArray2int,
  protocal_head,
  ObjectIdFlag
} from "./protocol.js"
import db from "./db3.js"
import message from './serialPortUtil'
import Encode from "./encode.js"
/**
 * 分发器
 */
class DispatchHandler {
  constructor() {
    this.map = new Map()
    this.map.set(ObjectIdFlag._heart, this.heart)
    this.map.set(ObjectIdFlag._phase, this.phase)
    this.map.set(ObjectIdFlag._lame, this.lamp)
    this.map.set(ObjectIdFlag._security, this.security)
  }
  // 联机请求
  heart(result, dataLength, content) {
    let time = takeByteArray2int(content.slice(0, 4))
    let communication_model = content[4]
    let control_model = content[5]
    console.log('heart=', time, communication_model, control_model)
    message.send(new Encode(result).response())
  }

  // 相位
  phase(result, dataLength, content) {
    // 相位
    let N = 49
    if (dataLength % N === 0) {
      let sql = ''
      let crossNumber = takeCrossNumber(result)
      let c = dataLength / N
      let j = 0
      for (let i = 0; i < c; i++) {
        // 灯组编号
        let binarystr = takeIntArray2binary(content.slice(1, 9))
        let lampNum = 0
        for (let i = 0; i < 64; i++) {
          if (((binarystr >> i) & 1) === 1) {
            lampNum = i + 1
            break
          }
        }
        console.log("灯组编号=", lampNum)
        let red = content[j + 10]
        let yellow = content[j + 12]
        let green = content[j + 14]
        let minGreen = takeIntArray2binary(content.slice(j + 34, j + 36))
        sql += `
        INSERT INTO phase(cross_number, phase_num, lamp_num, green, yellow, red, min_green) 
        VALUES (${crossNumber}, ${content[j]}, ${lampNum}, ${green}, ${yellow}, ${red}, ${minGreen});
        `
        j += N
      }
      db.multi(sql, (error, result) => { })
    } else {
      console.log("相位数据异常")
    }
  }

  // 灯组
  lamp(result, dataLength, content) {
    let crossNumber = takeCrossNumber(result)
    // 灯组
    let N = 3
    if (dataLength % N === 0) {
      let sql = ""
      let c = dataLength / N
      let j = 0
      for (let i = 0; i < c; i++) {
        sql += `INSERT INTO lamp (cross_number, lamp_num, direction, flow) 
        VALUES (${crossNumber}, ${content[j]}, ${content[j + 1]}, ${content[j + 2]});`
        j += N
      }
      db.multi(sql, (error, result) => { })
    } else {
      console.log("灯组数据异常")
    }
  }

  // 安保
  security(result, dataLength, content) {
    let crossNumber = takeCrossNumber(result)
    // 相位安保
    let N = 5
    if (dataLength % N === 0) {
      let sql = ''
      let c = dataLength / N
      let j = 0
      for (let i = 0; i < c; i++) {
        sql += `
        INSERT INTO security (cross_number, phase_num, security_green, security_green_flash, security_yellow, security_red)
        VALUES (${crossNumber}, ${content[j]}, ${content[j + 1]}, ${content[j + 2]}, ${content[j + 3]}, ${content[j + 4]});
        `
        j += N
      }
      db.multi(sql, (error, resutl) => { })
    } else {
      console.log("相位安保数据异常")
    }
  }

  /**
   * 
   * 分发
   */
  handler(res) {
    console.log("dispatch", res)
    let result = res.data
    // 实际数据内容长度
    let dataLength = res.dataLength
    // 实际数据内容
    let content = res.content
    this.map.get(takeObjectId(result))(result, dataLength, content)
  }
}

export default new DispatchHandler()
