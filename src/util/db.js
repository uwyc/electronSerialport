// const { ipcRenderer } = require('electron')
const Store = require('electron-store')
const storeOption = {
  name: 'zhjt-db',
  fileExtension: 'json',
  // cwd: window.zhjtData.userData,
  clearInvalidConfig: true
}
const store = new Store(storeOption)
store.set('phase', [])
export default store