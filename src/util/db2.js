const mysql = require('mysql')
const config = {
  host: 'localhost',
  user: 'root',
  port: 5369,
  password: '123456',
  database: 'tes'
}
class Db {
  constructor() {
    this.connection = mysql.createConnection(config)
    this.connect()
  }

  connect() {
    if (this.connection) {
      this.connection.connect((error,args)=>{
        console.log('db connent error ',error);
        console.log('db connent args ',args);        
        console.log('db connect stete=', this.connection.state)
      })
    }
  }

  close() {
    this.connection.end()
  }
  insert(sql, val, callback) {
    this.select(sql, val, callback)
  }
  delete(sql, val, callback) {
    this.select(sql, val, callback)
  }
  update(sql, val, callback) {
    this.select(sql, val, callback)
  }
  select(sql, val, callback) {
    console.log('执行 sql= ', sql);
    console.log('执行 val= ', val);
    this.connection.query(sql, val, (error, results, fields) => {
      console.log('执行 results= ', results);
      callback(error, results)
    })
  }
}
export default new Db()
