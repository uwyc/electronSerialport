const mysql = require('mysql')
class Db {
  constructor() {
    this.pool = mysql.createPool({
      connectionLimit: 5,
      host: 'localhost',
      port: 5369,
      user: 'root',
      password: '123456',
      database: 'tes',
      multipleStatements: true
    })
  }

  insert(sql, val, callback) {
    this.select(sql, val, callback)
  }
  delete(sql, val, callback) {
    this.select(sql, val, callback)
  }
  update(sql, val, callback) {
    this.select(sql, val, callback)
  }
  select(sql, val, callback) {
    console.log('执行 sql= ', sql);
    console.log('执行 val= ', val);
    this.pool.getConnection((error, connection) => {
      if (error) throw error
      connection.query(sql, val, (err, results, fields) => {
        callback(err, results)
        connection.release();
      })
    })
  }

  multi(sql, callback) {
    console.log('执行 sql= ', sql);
    this.pool.getConnection((error, connection) => {
      if (error) throw error
      connection.query(sql, (error, results, fields) => {
        callback(error, results)
        connection.release();
      })
    })

  }
}
export default new Db()
