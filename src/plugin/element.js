import Vue from 'vue'
import {  
  Message,
  MessageBox,
  Button,
  Form,
  FormItem,
  Input,
  Table,
  TableColumn,
  Card,
  Breadcrumb,BreadcrumbItem,
  Select,Option,
  Dialog,
  Col,Row,
  Pagination
} from 'element-ui'

Vue.use(Button)
Vue.use(Form)
Vue.use(FormItem)
Vue.use(Input)
Vue.use(Table)
Vue.use(TableColumn)
Vue.use(Card)
Vue.use(Breadcrumb)
Vue.use(BreadcrumbItem)
Vue.use(Select)
Vue.use(Option)
Vue.use(Dialog)
Vue.use(Col)
Vue.use(Row)
Vue.use(Pagination)

Vue.prototype.$message = Message
Vue.prototype.$confirm = MessageBox.confirm
