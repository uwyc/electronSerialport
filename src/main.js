import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
// import ElementUI from 'element-ui';
// Vue.use(ElementUI)
import '@/plugin/element.js'
import 'element-ui/lib/theme-chalk/index.css';

import { directionEnum, flowEnum, channelEnum } from "@/util/ComEnum.js";

Vue.filter('DirectionFilter', (val) => directionEnum.get(val))
Vue.filter('FlowFilter', (val) => flowEnum.get(val))
Vue.filter('ChannelFilter', (val) => channelEnum.get(val))

Vue.config.productionTip = false
new Vue({
  router, store, render: h => h(App)
}).$mount('#app')
