import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'

Vue.use(VueRouter)

const routes = [
  {
    path: '/home',
    name: 'Home',
    component: Home,
    redirect: '/stage',
    children: [
      {
        path: '/phase',
        name: 'Phase',
        component: () => import('../views/Phase.vue')
      },
      {
        path: '/stage',
        name: 'Stage',
        component: () => import('../views/Stage.vue')
      },
      {
        path: '/chain',
        name: 'Chain',
        component: () => import('../views/Chain.vue')
      },
      {
        path: '/daily/plan',
        name: 'DailyPlan',
        component: () => import('../views/DailyPlan.vue')
      },
      {
        path: '/dispatch',
        name: 'Dispatch',
        component: () => import('../views/Dispatch.vue')
      },
      {
        path: '/network',
        name: 'Network',
        component: () => import('../views/Network.vue')
      }

    ]
  },
  {
    path: '/about',
    name: 'About',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import(/* webpackChunkName: "about" */ '../views/About.vue')
  }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
