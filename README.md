# electronSerialport

#### 介绍
vue+electron+serialport

#### 软件架构
软件架构说明


#### 安装教程

##### 1.准备工作，访问serialport(https://serialport.io/docs/guide-installation#windows) 官网，查看windowns下 serialport 安装说明

```
使用 node-gyp 安装，安装说明地址 https://github.com/nodejs/node-gyp#installation，以下是执行步骤

1.npm install -g node-gyp
2.安装 visual studio build tools
3.npm config set msvs_version 2017
4.安装 python3.9.9
5.npm config set python D:\Program Files\Python\Python39\python.exe
```



##### 2.访问 github 官网下载  electron-serialport 项目示例。

```
1.electron-serialport 项目下载地址
https://github.com/serialport/electron-serialport
2.修改项目的 package.json 文件,将 electron，electron-rebuild，serialport 改为最新版本。
{
  "devDependencies": {
        "electron": "^13.6.6",
        "electron-rebuild": "^3.2.6"
    },
    "dependencies": {
        "serialport": "^10.0.1",
        "tableify": "^1.1.0"
    }
}
3. 执行 npm install 安装依赖，当出现 Rebuild Complete 说明成功
4. 执行 npm start 启动姓名。
```

##### 3.vue 项目构建成 electron 项目

1. 创建 vue 项目
```
  vue create xxx
```

2. 安装 vue-cli-plugin-electron-builder 插件

通过该插件将 vue 项目改造成 electron项目

```
vue add electron-builder
```

##### 4. node.js 安装 mysql 客户端
```
  # 官网地址 https://github.com/mysqljs/mysql
  npm install mysql
```
